# To run:

~~~
docker network create nginx-proxy
docker-compose up -d
~~~

# To enable ssl for domain(s) in your project:
- add '--network nginx-proxy' to proxied containers
- setup environment variables VIRTUAL_HOST, LETSENCRYPT_HOST and LETSENCRYPT_EMAIL.

Example:
~~~
version: '3'

services:
  app:
    ...

  nginx:
    image: nginx:stable-alpine
    expose:
      - 80
    volumes:
      - .:/var/www
      - ./docker/nginx/nginx.conf:/etc/nginx/nginx.conf
      - ./docker/nginx/conf.d/default.conf:/etc/nginx/conf.d/default.conf
      - ./docker/nginx/conf.d/_http.conf:/etc/nginx/conf.d/_http.conf
    depends_on:
      - php
    restart: always
    environment:
      - VIRTUAL_HOST=mydomain1.com,mydomain2.com,admin.mydomain1.com
      - LETSENCRYPT_HOST=mydomain1.com,mydomain2.com,admin.mydomain1.com
      - LETSENCRYPT_EMAIL=andrey.valiev@mymechanic.ru

networks:
  default:
    external:
      name: nginx-proxy
~~~
